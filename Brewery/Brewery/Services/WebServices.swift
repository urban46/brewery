//
//  WebServices.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation
import Alamofire


class WebService {

    static func logIn(data: LogIn, handler: @escaping (LoginResponse?) -> Void) {
        
        let url = URL(string: "https://www.reintodev.cz:4006/api/login")
        let headers : [String: String] = ["Content-Type":"application/json"]
        
        Alamofire.request(url!, method: .post, parameters: data.jsonLogin,encoding: JSONEncoding.default, headers: headers).responseJSON
        { response in
            switch(response.result)
            {
                case .success( _):
                    if let status = response.response?.statusCode,
                        status == 200, let json = response.data,
                         let parsed = try? JSONDecoder().decode(LoginResponse.self, from: json) {
                         handler(parsed)
                    } else {
                        handler(nil)
                    }
                case .failure( _):
                    handler(nil)
            }
        }
    }
    
    static func getData(userId: Int, handler: @escaping ([Breweries]) -> Void) {
        
        let url = URL(string: "https://www.reintodev.cz:4006/api/breweries?userId=\(userId)&skip=skip")
        let headers : [String: String] = ["Content-Type":"application/json"]
        
        Alamofire.request(url!, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON
            { response in
                switch(response.result)
                {
                case .success( _):
                    if let status = response.response?.statusCode, status == 200,
                        let json = response.data, let list = try? JSONDecoder().decode([Breweries].self, from: json)
                    {
                        handler(list)
                    } else {
                        handler([Breweries]())
                    }
                case .failure( _):
                    handler([Breweries]())
                }
        }
    }
    
}
