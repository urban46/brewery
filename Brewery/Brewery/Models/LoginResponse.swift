//
//  LoginResponse.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation

class LoginResponse : Decodable {
    var userId: Int
}
