//
//  Breweries.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation

class Breweries: Decodable {
    var id: Int?
    var name: String?
    var url: String?
}


