//
//  LoginModel.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation

class LogIn {
    
    private(set) var username: String?
    private(set) var password: String?
    
    init(username: String?, password: String?) {
        self.username = username
        self.password = password
    }
    
    var jsonLogin:[String: String] {
        return ["login" : self.username ?? "", "password" : self.password ?? "" ]
    }
}
