//
//  TableViewCell.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation
import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnShow: UIButton!
    
    var delegate: TableViewCellShowDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func clicTableButtonShow(_ sender: Any) {
        if let path = indexPath {
            self.delegate?.showUrl(at: path)
        }
    }
}

protocol TableViewCellShowDelegate {
    func showUrl(at index:IndexPath)
}

extension UITableViewCell{
    
    var tableView:UITableView?{
        return superview as? UITableView
    }
    
    var indexPath:IndexPath? {
        return tableView?.indexPath(for: self)
    }
}
