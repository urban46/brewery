//
//  ViewController.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    private var provider: ILogin!
    private var loginResponse: LoginResponse?
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        provider = LoginLogic()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loginResponse = nil
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return loginResponse != nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? ListBreweriesViewController, loginResponse != nil {
            vc.userId = loginResponse?.userId
        }
    }
    
    
    @IBAction func lgnClicked(_ sender: Any) {
        
        let logindata = LogIn(username: userName.text, password: password.text)
        let validationResult = ValidationModelManager.validateLogin(model: logindata)
        
        if (validationResult.validationError) {
            showToast(message: validationResult.validationMessage)
            return
        }

        provider?.logIn(data: logindata, handler: { data in
            
            if data != nil {
                self.loginResponse = data
                self.performSegue(withIdentifier: "loginSuccessful", sender: self)
            } else {
                self.showToast(message: ValidationModel.ErrorMessages.incorrectLogin)
            }
        })
    }
}

