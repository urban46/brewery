//
//  ListBreweries.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import UIKit

class ListBreweriesViewController: UIViewController {
    
    var userId: Int!
    private var data: [Breweries]! {
        didSet {
            self.dataTableView.reloadData()
        }
    }
    
    @IBOutlet weak var dataTableView: UITableView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        data = [Breweries]()
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "List of Breweries"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        WebService.getData(userId: self.userId, handler: { data in
            self.data = data
        })
    }
}

extension ListBreweriesViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listIdTableCell", for: indexPath) as! TableViewCell
        cell.lblName.text = data[indexPath.row].name
        cell.delegate = self 
        return cell
    }
}

extension ListBreweriesViewController: TableViewCellShowDelegate {
    func showUrl(at index: IndexPath) {
        if let url = data[index.row].url {
            guard let urlData = URL(string:  url) else { return }
            UIApplication.shared.open(urlData)
        }
    }
}
