//
//  LoginLogic.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation

protocol ILogin {
    func logIn(data: LogIn, handler: @escaping (LoginResponse?) -> Void)
}

class LoginLogic: ILogin {
    
    func logIn(data: LogIn, handler: @escaping (LoginResponse?) -> Void) {
        WebService.logIn(data: data, handler: handler)
    }
}
