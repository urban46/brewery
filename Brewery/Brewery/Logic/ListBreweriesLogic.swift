//
//  ListBreweriesLogic.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation

protocol IListBreweriesLogic {
    func refreshFromServer(handler: @escaping (Bool) -> Void)
}

class ListBreweriesLogic: IListBreweriesLogic {
    func refreshFromServer(handler: @escaping (Bool) -> Void) {
        debugPrint("test")
    }
}
