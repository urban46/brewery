//
//  ValidationManager.swift
//  Brewery
//
//  Created by patrikurban on 24/11/2018.
//  Copyright © 2018 Patrik Urban. All rights reserved.
//

import Foundation

class ValidationModel {
    
    var validationError: Bool!
    var validationMessage: String!
    
    init(validationError: Bool, validationMessage: String) {
        
        self.validationError = validationError
        self.validationMessage = validationMessage
    }

    class ErrorMessages {
        static let msgEmptyEmail = "Please enter your username"
        static let msgEmptyPassword = "Please enter your password"
        static let incorrectLogin = "Incorrect login credential"
    }
}

class ValidationModelManager {
    
    static func validateLogin(model: LogIn) -> ValidationModel
    {
        var result = ValidationModel(validationError: false, validationMessage: "")
        // username string empty
        if model.username == "" || model.username == nil
        {
            result = ValidationModel(validationError: true, validationMessage: ValidationModel.ErrorMessages.msgEmptyEmail)
        }
        // passwords string empty
        if model.password == "" || model.password == nil
        {
            result = ValidationModel(validationError: true, validationMessage: ValidationModel.ErrorMessages.msgEmptyPassword)
        }
        
        return result
    }
}
